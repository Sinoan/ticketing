﻿using System;
using System.Linq;
using Domain;

namespace Domain_Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            ShowDatas();
        }

        static void ShowDatas(){
            using(var context = new TicketingContext()) {
                
                Console.WriteLine("-------- TICKETS -------");
                foreach(var ticket in context.Tickets){
                    Console.WriteLine($"{ticket.TicketId} | {ticket.Title} | {ticket.CreatedBy} | {ticket.DateCreated}");
                }
                Console.WriteLine("");
                Console.WriteLine("-------- USERS -------");

                foreach(var user in context.Users){
                    Console.WriteLine($"{user.UserId} | {user.FirstName} | {user.LastName} | {user.PhoneNumber}");
                }
                Console.WriteLine("");
                Console.WriteLine("-------- CLIENTS -------");

                foreach(var client in context.Clients){
                    Console.WriteLine($"{client.ClientId} | {client.Name} | {client.PhoneNumber} | {client.Address}");
                }
                Console.WriteLine("");
                Console.WriteLine("-------- ACTIONS -------");

                foreach(var action in context.Actions){
                    Console.WriteLine($"{action.ActionId} | {action.Title} | {action.UserId} | {action.ActionDate}");
                }
                
            }
        }
        static void AddClient(){
            using(var context = new TicketingContext()) {
                var NewClient = new Client{
                    Name= "John Doe",
                    PhoneNumber="0388984516",
                    Address="rue de Londres, 67670 Mommenheim"
                };
                context.Clients.Add(NewClient);
                context.SaveChanges();
            }
        }

        static void AddUser(){
            using(var context = new TicketingContext()) {
                var NewUser = new User{
                    FirstName= "Jean",
                    LastName="Muller",
                    PhoneNumber="0388984516",
                    Email="j.muller@gmail.com"
                };
                context.Users.Add(NewUser);
                context.SaveChanges();
            }
        }

        static void AddTicket(){
            using(var context = new TicketingContext()) {
                var NewTicket = new Ticket{
                    Object= "Ceci est un ticket avec un conte rendu de l'appel client",
                    Title="Titre du ticket",
                    ClientId = 1,
                    CreatedBy = 1
                };
                context.Tickets.Add(NewTicket);
                context.SaveChanges();
            }
        }

        static void AddAction(){
            using(var context = new TicketingContext()) {
                var NewAction = new Domain.Action{
                    Description= "Ceci est une action avec un conte rendu de la correction apportée",
                    Title="Titre de l'action",
                    UserId = 1,
                    TicketId = 7
                };
                context.Actions.Add(NewAction);
                context.SaveChanges();
            }
        }
    }
}
