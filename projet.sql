﻿-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.3.7-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la base pour ticketsystem
CREATE DATABASE IF NOT EXISTS `ticketsystem` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ticketsystem`;

Drop table IF  EXISTS `t_user`;
-- Export de la structure de la table ticketsystem. t_user
CREATE TABLE IF NOT EXISTS `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Export de données de la table ticketsystem.t_user
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` (`user_id`, `first_name`, `last_name`, `phone_number`, `email`) VALUES
	(1, 'Romain', 'SPIELMANN', '0612345600', 'rst@gmail.com'),
        (2, 'Stéphane', 'CAMUSSO', '06412345620', 'Staf@yahoo.fr'),
        (3, 'Robin', 'BOLLE', '0612345610', 'robin@hotmail.fr'),
        (4, 'Vincent', 'HOEN', '0699123456', 'vincent@hotmail.fr'),
        (5, 'Alain', 'Jantet', '0612345622', 'aj@gmail.com'),
        (6, 'Guillaume', 'Cravis', '0612345611', 'gg@gmail.com'),
        (7, 'Alex', 'Cris', '0612345611', 'alex@gmail.com');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;

Drop table IF  EXISTS `t_ticket`;
-- Export de la structure de la table ticketsystem. t_ticket
CREATE TABLE IF NOT EXISTS `t_ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_close` datetime DEFAULT NULL,
  `object` varchar(255) NOT NULL,
  `date_Create` datetime NOT NULL DEFAULT current_timestamp(),
  `title` varchar(30) NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `closed_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `FK_client_id` (`client_id`),
  KEY `FK_t_ticket_t_user` (`created_by`),
  CONSTRAINT `FK_client_id` FOREIGN KEY (`client_id`) REFERENCES `t_client` (`client_id`),
  CONSTRAINT `FK_t_ticket_t_user` FOREIGN KEY (`created_by`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Export de données de la table ticketsystem.t_ticket : ~2 rows (environ)
/*!40000 ALTER TABLE `t_ticket` DISABLE KEYS */;
INSERT INTO `t_ticket` (`ticket_id`, `date_close`, `object`, `date_Create`, `title`, `client_id`, `created_by`, `closed_by`) VALUES
	(1, '2018-09-11 10:40:00', 'Incident', '2018-09-17 10:36:35', 'Bug applicatif', 3, 1, 2),
	(2, NULL, 'Incident', '2018-09-17 10:41:03', 'Pas de réponse serveur', 4, 1, NULL),
	(3,  NULL, 'Retard', '2018-09-17 10:36:35', 'Livraison en retard', 5, 1, 2),
	(4, NULL, 'Incident', '2018-09-17 10:41:03', 'Sauvegarde a échoué', 2, 1, NULL),
	(5,  NULL, 'Incident', '2018-09-17 10:36:35', 'Disque dur en panne', 3, 1, 2),
	(6, NULL, 'Incident', '2018-09-17 10:41:03', 'Panne réseau', 1, 2, NULL),
	(7, '2018-09-18 10:51:00', 'Incident', '2018-06-17 10:36:35', 'Panne réseau de 10h à 12h', 1, 1, 2),
	(8, NULL, 'Incident', '2018-09-17 10:41:03', 'Panne du logiciel', 1, 1, NULL),
	(9, '2018-09-19 10:01:01', 'Incident', '2018-09-18 10:36:35', 'Pas de toner pour imprimer', 1, 1, 2),
	(10, '2018-02-20 10:41:16', 'Incident', '2018-02-17 10:01:03', '??', 1, 2, NULL),
	(11, '2018-03-20 10:41:16', 'Incident', '2018-03-17 10:03:35', 'Panne imprimante 210', 3, 1, 2),
	(12, '2018-09-28 10:51:26', 'Incident', '2018-05-17 10:01:03', 'Panne imprimante', 3, 1, NULL),
	(13, NULL, 'Incident', '2018-04-17 11:36:35', 'Panne imprimante 210', 1, 1, 2),
	(14, NULL, 'Incident', '2018-09-17 10:41:03', '...',2, 1, NULL),
	(15, '2018-09-17 12:01:01', 'Incident', '2018-09-17 10:36:35', 'Panne imprimante 210', 2, 1, 2),
	(16, NULL, 'Incident grave', '2018-09-17 10:41:03', 'Panne impression ', 1, 1, NULL);
/*!40000 ALTER TABLE `t_ticket` ENABLE KEYS */;




Drop table IF  EXISTS `t_action`;
-- Export de la structure de la table ticketsystem. t_action
CREATE TABLE IF NOT EXISTS `t_action` (
  `action_id` int(11) NOT NULL AUTO_INCREMENT,
  `action_date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`action_id`),
  KEY `FK_user_id` (`user_id`),
  KEY `FK_ticket_id` (`ticket_id`),
  CONSTRAINT `FK_ticket_id` FOREIGN KEY (`ticket_id`) REFERENCES `t_ticket` (`ticket_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `t_action` (`action_id`,  `action_date`, `description`,`user_id`,`ticket_id`,`title`) VALUES
(1,  '2018-09-17 10:41:03', 'Intervention 1',1,1,'Titre interv. N° 1'),
(2,  '2018-09-17 10:41:03', 'Réparation',1,7,'Titre interv. N° 2'),
(3,  '2018-09-17 10:41:03', 'Réparation',1,9,'Titre interv. N° 3'),
(4,  '2018-09-17 10:41:03', 'Réparation',1,10,'Titre interv. N° 4'),
(5,  '2018-09-17 10:41:03', 'Intervention',1,11,'Description interv. N° 5'),
(6,  '2018-09-17 10:41:03', 'Intervention',1,12,'Titre interv. N° 6'),
(7, '2018-09-17 10:41:03', 'Intervention',1,15,'Description interv. N° 7');
/*!40000 ALTER TABLE `t_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_action` ENABLE KEYS */;
-- Export de la structure de la table ticketsystem. t_client
CREATE TABLE IF NOT EXISTS `t_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

Drop table IF  EXISTS `t_client`;
-- Export de données de la table ticketsystem.t_client : ~1 rows (environ)
-- Export de la structure de la table ticketsystem. t_client
CREATE TABLE IF NOT EXISTS `t_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `t_client` DISABLE KEYS */;
INSERT INTO `t_client` (`client_id`, `name`, `phone_number`, `address`) VALUES
	(1, 'CESI', '0612345610', ' Parc Club des Tanneries - 67380 Strasbourg Lingolsheim'),
        (2, 'Hopital Civil', '0615015600', '8 rue OSLO 67200 Strasbourg'),
        (3, 'CHRU', '0610345600', '10 rue Grantt  67200 Strasbourg'),
        (4, 'Business Red', '0602145610', '1 rue Vede 40200 Mont de Marsan'),
        (5, 'SFR', '0610345600', '8 rue verte 75200 Paris'),
        (6, 'Xerox', '0612045601', '1000 boulevard Malraux 37000 Tours'),
        (7, 'Sté Zend', '0612340600', '11 rue de Montagne verte 75000 Paris');
/*!40000 ALTER TABLE `t_client` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

