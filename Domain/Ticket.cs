using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    [Table("t_ticket")]
    public class Ticket{
        [Column("ticket_id")]
        public int TicketId{get;set;}
        [Column("date_create")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateCreated{get;set;}
        [Column("date_close")]
        public DateTime? DateClosed{get;set;}
        [Column("object")]
        public string Object{get;set;}
        [Column("title")]
        public string Title{get;set;}
        [Column("client_id")]
        public int ClientId{get;set;}
        [Column("created_by")]
        public int CreatedBy{get;set;}
        [Column("closed_by")]
        public int? ClosedBy{get;set;}

        public Client Client{get;set;}
        [ForeignKey("CreatedBy")]
        public User UserWhoCreated{get;set;}
        [ForeignKey("ClosedBy")]
        public User UserWhoClosed{get;set;}
    }
}