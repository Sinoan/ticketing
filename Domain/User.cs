using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    [Table("t_user")]
    public class User{
        [Column("user_id")]
        public int UserId{get;set;}

         [Column("first_name")]
        public string FirstName{get;set;}

         [Column("last_name")]
        public string LastName{get;set;}

         [Column("phone_number")]
        public string PhoneNumber{get;set;}

         [Column("email")]
        public string Email{get;set;}
    }
}