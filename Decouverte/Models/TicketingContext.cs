using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql;

namespace Decouverte.Models {
    public class TicketingContext : DbContext {
        public DbSet<Ticket> Tickets {get; set;}

        public DbSet<User> Users {get; set;}

        public DbSet<Action> Actions {get; set;}

        public DbSet<Client> Clients {get; set;}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){
            optionsBuilder.UseMySql("SERVER=localhost;UID=root;DATABASE=ticketsystem");
            // optionsBuilder.UseMySql("SERVER=localhost;UID=root;PASSWORD=root;DATABASE=ticketsystem");
        }

    }
}