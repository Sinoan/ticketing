using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Decouverte.Models
{
    [Table("t_client")]
    public class Client{
        [Column("client_id")]
        public int ClientId{get;set;}
        
        [Column("name")]
        public string Name{get;set;}
        
        [Column("phone_number")]
        public string PhoneNumber{get;set;}
        
        [Column("address")]
        public string Address{get;set;}

        public ICollection<Ticket> Tickets  {get; set;}
        
        public override string ToString() {
            return this.Name;
        }
    }
}