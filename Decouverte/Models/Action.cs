using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Decouverte.Models
{
    [Table("t_action")]
    public class Action{
        [Column("action_id")]
        public int ActionId{get;set;}

        [Column("action_date")]
        public DateTime ActionDate{get;set;}
        
        [Column("description")]
        public string Description{get;set;}
        
        [Column("user_id")]
        public int UserId{get;set;}
        
        [Column("ticket_id")]
        public int TicketId{get;set;}
        
        [Column("title")]
        public string Title{get;set;}

        public User User{get;set;}
        public Ticket Ticket{get;set;}
    }
}