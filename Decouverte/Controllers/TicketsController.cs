using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Decouverte.Models;
using Microsoft.EntityFrameworkCore;

namespace Decouverte.Controllers
{
    public class TicketsController : Controller
    {
        [HttpGet]
        public IActionResult Index(){
            List<Ticket> mytickets;
            using(var context = new TicketingContext()) {
                mytickets = context.Tickets.Include(t => t.Client).ToList();
            }
            mytickets = mytickets.OrderBy(e => e.DateClosed).ToList();
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            this.ViewData["UserList"] = myUsers;
            return View(mytickets);
        }
        [HttpGet]
        public IActionResult Read(int id){
            List<Ticket> myTickets;
            List<Models.Action> myActions;
            using(var context = new TicketingContext()) {
                myTickets = context.Tickets.Include(t => t.Client)
                                            .Include(t => t.UserWhoCreated)
                                            .Include(t => t.UserWhoClosed)
                                            .ToList();
                var Ticket = myTickets.Find(e => e.TicketId == id);
                if(Ticket == null)
                    return NotFound($"Le ticket {id} n'existe pas!");

                myActions = context.Actions.Include(t => t.User)
                                            .Where(e => e.TicketId == Ticket.TicketId)
                                            .OrderByDescending(e => e.ActionDate)
                                            .ToList();
                this.ViewData["ActionList"] = myActions;
                return View(Ticket);
            }
        }

        [HttpGet]
        public IActionResult Edit(int id){
            List<Ticket> mytickets;
            using(var context = new TicketingContext()) {
                mytickets = context.Tickets.ToList();
            }
            var Ticket = mytickets.Find(e => e.TicketId == id);
            if(Ticket == null)
                return NotFound($"le ticket {id} n'existe pas!");
            return View(Ticket);
        }

        [HttpPost]
        public IActionResult Edit(int id, Ticket ticket){
            List<Ticket> mytickets;
            using(var context = new TicketingContext()) {
                mytickets = context.Tickets.ToList();
                var old = mytickets.Find(e => e.TicketId == id);
                if(old == null){
                    return NotFound($"le ticket {id} n'existe pas!"); 
                }
                if(ModelState.IsValid){
                    string ActionDescription = "";
                    if (old.Title != ticket.Title)
                        ActionDescription += "Changement du titre - Ancien titre : " + old.Title + "  -  Nouveau titre : " + ticket.Title;
                    if (old.Object != ticket.Object)
                        ActionDescription += "Changement de l'objet - Ancien objet : " + old.Object + "  -  Nouvel objet : " + ticket.Object;
                    old.Title = ticket.Title;
                    old.Object = ticket.Object;
                    var NewAction = new Models.Action{
                        TicketId = old.TicketId,
                        ActionDate = DateTime.Now,
                        UserId = 1,
                        Title = "Modification du Ticket",
                        Description = ActionDescription,
                    };
                    context.Actions.Add(NewAction);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(old);
            }
        }
        [HttpGet]
        public IActionResult Add(){
            List<Client> myCustomers;
            using(var context = new TicketingContext()) {
                myCustomers = context.Clients.ToList();
            }
            this.ViewData["ClientList"] = myCustomers;
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            this.ViewData["UserList"] = myUsers;
            return View();
        }
        [HttpPost]
        public IActionResult Add(Ticket ticket){
            if(ModelState.IsValid){
                using(var context = new TicketingContext()) {
                    var NewTicket = new Ticket{
                        Object= ticket.Object,
                        Title= ticket.Title,
                        DateCreated = DateTime.Now,
                        ClientId = ticket.ClientId,
                        CreatedBy = ticket.CreatedBy
                    };
                    context.Tickets.Add(NewTicket);
                    context.SaveChanges();
                    var NewAction = new Models.Action{
                        TicketId = NewTicket.TicketId,
                        ActionDate = DateTime.Now,
                        UserId = 1,
                        Title = "Création du Ticket",
                        Description = "Le ticket à été créé",
                    };
                    context.Actions.Add(NewAction);
                    context.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        public IActionResult Close(int id, int userId){
            List<Ticket> mytickets;
            using(var context = new TicketingContext()) {
                mytickets = context.Tickets.ToList();
            
                var Ticket = mytickets.Find(e => e.TicketId == id);

                if(Ticket == null)
                    return NotFound($"Le ticket {id} n'existe pas!");

                Ticket.ClosedBy = userId;
                Ticket.DateClosed = DateTime.Now;
                var NewAction = new Models.Action{
                    TicketId = Ticket.TicketId,
                    ActionDate = DateTime.Now,
                    UserId = userId,
                    Title = "Fermeture du Ticket",
                    Description = "Le ticket à été fermé",
                };
                context.Actions.Add(NewAction);
                context.SaveChanges();
                
            }
            return RedirectToAction("Index");
        }
    }
}
