using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Decouverte.Models;

namespace Decouverte.Controllers
{
    public class ActionController : Controller
    {
        [HttpGet]
        public IActionResult Add(int id){
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            this.ViewData["UserList"] = myUsers;
            this.ViewData["TicketId"] = id;
            return View();
        }
        [HttpPost]
        public IActionResult Add(Models.Action actionObject){
            if(ModelState.IsValid){
                using(var context = new TicketingContext()) {
                    var NewAction = new Models.Action{
                        Description = actionObject.Description,
                        Title = actionObject.Title,
                        UserId = actionObject.UserId,
                        TicketId = actionObject.TicketId,
                        ActionDate = DateTime.Now,
                    };
                    actionObject.ActionDate = DateTime.Now;
                    context.Actions.Add(actionObject);
                    context.SaveChanges();
                }
                return RedirectToAction("Read", "Tickets", new { id = actionObject.TicketId });
            }
            return View();
        }
    }
}