using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Decouverte.Models;

namespace Decouverte.Controllers
{
    public class ClientController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            List<Client> myClients;
            using (var context = new TicketingContext())
            {
                myClients = context.Clients.ToList();
            }
            Console.WriteLine(myClients);
            return View(myClients);
        }
        [HttpGet]
        public IActionResult Read(int id)
        {
            List<Client> myClients;
            using (var context = new TicketingContext())
            {
                myClients = context.Clients.ToList();
            }
            var Client = myClients.Find(e => e.ClientId == id);
            if (Client == null)
                return NotFound($"Le client {id} n'existe pas!");
            return View(Client);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            List<Client> myClients;
            using (var context = new TicketingContext())
            {
                myClients = context.Clients.ToList();
            }
            var Client = myClients.Find(e => e.ClientId == id);
            if (Client == null)
                return NotFound($"Le client {id} n'existe pas!");
            return View(Client);
        }

        [HttpPost]
        public IActionResult Edit(int id, Client Client)
        {
            List<Client> myClients;
            using (var context = new TicketingContext())
            {
                myClients = context.Clients.ToList();

                var old = myClients.Find(e => e.ClientId == id);
                if (old == null)
                {
                    return NotFound($"le client {id} n'existe pas!");
                }
                if (ModelState.IsValid)
                {
                    old.Name = Client.Name;
                    old.Address = Client.Address;
                    old.PhoneNumber = Client.PhoneNumber;
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(old);
            }
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(Client Client)
        {
            if (ModelState.IsValid)
            {
                using (var context = new TicketingContext())
                {
                    var NewClient = new Client
                    {
                        Name = Client.Name,
                        Address = Client.Address,
                        PhoneNumber = Client.PhoneNumber,
                    };
                    context.Clients.Add(NewClient);
                    context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        public IActionResult Delete(int id)
        {
            List<Client> myClients;
            using (var context = new TicketingContext())
            {
                myClients = context.Clients.ToList();

                var Client = myClients.Find(e => e.ClientId == id);

                if (Client == null)
                    return NotFound($"Le client {id} n'existe pas!");

                context.Remove(Client);
                context.SaveChanges();

            }
            return RedirectToAction("Index");
        }
    }
}