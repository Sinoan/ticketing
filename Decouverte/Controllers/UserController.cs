using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Decouverte.Models;

namespace Decouverte.Controllers
{
    public class UserController : Controller
    {
        
        [HttpGet]
        public IActionResult Index(){
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            Console.WriteLine(myUsers);
            return View(myUsers);
        }
         [HttpGet]
        public IActionResult Read(int id){
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            var User = myUsers.Find(e => e.UserId == id);
            if(User == null)
                return NotFound($"L'utilisateur {id} n'existe pas!");
            return View(User);
        }

        [HttpGet]
        public IActionResult Edit(int id){
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            }
            var User = myUsers.Find(e => e.UserId == id);
            if(User == null)
                return NotFound($"L'utilisateur {id} n'existe pas!");
            return View(User);
        }

        [HttpPost]

        [HttpPost]
        public IActionResult Edit(int id, User user)
        {
            List<User> myUsers;
            using (var context = new TicketingContext())
            {
                myUsers = context.Users.ToList();

                var old = myUsers.Find(e => e.UserId == id);
                if (old == null)
                {
                    return NotFound($"L'utilisateur {id} n'existe pas!");
                }
                if (ModelState.IsValid)
                {
                    old.Email = user.Email;
                    old.FirstName = user.FirstName;
                    old.LastName = user.LastName;
                    old.PhoneNumber = user.PhoneNumber;
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(old);
            }
        }
        [HttpGet]
        public IActionResult Add(){
            return View();
        }
        [HttpPost]
        public IActionResult Add(User user){
            if(ModelState.IsValid){
                using(var context = new TicketingContext()) {
                    var NewUser = new User{
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                    };
                    context.Users.Add(NewUser);
                    context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
        
        public IActionResult Delete(int id){
            List<User> myUsers;
            using(var context = new TicketingContext()) {
                myUsers = context.Users.ToList();
            
                var user = myUsers.Find(e => e.UserId == id);

                if(user == null)
                    return NotFound($"L'utilisateur {id} n'existe pas!");

                context.Remove(user);
                context.SaveChanges();
                
            }
            return RedirectToAction("Index");
        }
    }
}